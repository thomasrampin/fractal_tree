#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <cstdlib>
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace {
void CreateTreeFractal(float x, float y, float width, float height, float rotation){

	if (height < 5 || rotation > 135 || rotation < -135)

		return;
	RectangleShape node = new RectangleShape(new Vector2f(width, height));
	node.Origin = new Vector2f(width * 0.5f, height);
	node.Position = new Vector2f(x, y);
	node.Rotation = rotation;
	if (height < 20)
		node.FillColor = new Color(0, (byte)(rnd.Next(100, 256)), 0);
	Nodes.Add(node);
	// Calculate position for next node.
	double dx = height * Math.Sin(rotation * (Math.PI / 180));
	double dy = height * Math.Cos(rotation * (Math.PI / 180));

	CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation + rnd.Next(15, 75));
	if (rnd.Next(100) > 60)
	{
		CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation);
	}
	CreateTreeFractal(x + (float)dx, y - (float)dy, width * 0.8f, height * 0.75f, rotation - rnd.Next(15, 75));
	}
List<Shape> Nodes {get;set;}

Random rnd = new Random((int)(DateTime.Now.Ticks));

}
