#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

using namespace sf;
using namespace std;

namespace posey{

	vector<float> DiametrePosey(void){

		float diametre = 50; // choix du diametre
		vector<float> TableDiametre;
		//cout<<"Le diametre est de : "<< diametre << endl;
		TableDiametre.push_back(diametre);
		for(int i=1 ; i<10 ; i++)/*For pour remplir le tableau, c'est pas fait pour afficher, il est normal de ne pas voir le 4*/
		{
			diametre= diametre/2;
			if (diametre < 0)
				diametre =+ 1;
			TableDiametre.push_back(diametre);
			//cout << TableDiametre[i]<<endl;
		}
		//cout<<'\n';
		return TableDiametre;
	}

	vector<float> LongueurPez(void){
		float alpha = 2;
		float laPuissance = -3/(pow(2,alpha));
		float facteur = pow(2,laPuissance);
		float longueur = 300; //choix de la longueur
		vector<float> TableLongueur;
		//cout<<longueur<<" man voici ta longueur, elle triple p�se"<<endl;
		TableLongueur.push_back(longueur);
		for(int i=1; i<10; i++)/*For pour remplir le tableau, c'est pas fait pour afficher, il est normal de ne pas voir le 10*/
		{
			longueur=facteur*longueur;
			if (longueur < 0)
				longueur =+ 1;
			TableLongueur.push_back(longueur);
			//cout<<TableLongueur[i]<<endl;
		}
		//cout<<'\n';
		return TableLongueur;
	}

	void RectangleTriplePez(vector<float> &TableDiametre, vector<float> &TableLongueur, RenderWindow &app){
		int compteur = 1;
		float height = 0;
		float width = 0;

		float pointDep = 500;
		Vector2f pointDepSeconRect(612+width,pointDep);

		RectangleShape rectangle;
		VertexArray Arbre (rectangle, compteur); //je sais que �a marche pas ....

		Vector2u size = app.getSize();
		int heightWindow = size.y;

		for(unsigned i = 0; i<10; i++){

			height=TableLongueur[i];
			width=TableDiametre[i];
			if (width < 5)
				rectangle.setFillColor(sf::Color(100, 250, 50));
			else
				rectangle.setFillColor(sf::Color(128, 64, 0));

			rectangle.setSize(Vector2f(width,height));

			if (pointDep + height > heightWindow){
				pointDep = heightWindow - height -10;
			}
			rectangle.setPosition(612,pointDep);
			if (i>=1){
				rectangle.rotate(20);

			}
			//Arbre.push_back(rectangle);

			pointDep-=TableLongueur[i+1];
			if (compteur != 512){
				app.draw(Arbre);
				compteur *= 2;
			}
			else {
				return;
			}
		}

		//return Arbre;
	}

}
int main()
{
	vector<float> TableLongueur;
	vector<float> TableDiametre;
	TableDiametre = posey::DiametrePosey();
	TableLongueur = posey::LongueurPez();
	RenderWindow app(VideoMode(1024, 768, 32), "Fractal Tree SFML, triple pesant de Verdure !"/*, Style::Fullscreen*/);
    // Boucle principale
    while (app.isOpen())
    {
        Event event;

        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed)
                app.close();
        }

        // Remplissage de l'�cran (couleur noire par d�faut)
        app.clear();
        //dessin de l'arbre
        posey::RectangleTriplePez(TableDiametre, TableLongueur, app);
        // Affichage de la fen�tre � l'�cran
        app.display();
    }
    return EXIT_SUCCESS;
	//return 0;
}
//pow(a,b) c'est le carr� de a^b !
